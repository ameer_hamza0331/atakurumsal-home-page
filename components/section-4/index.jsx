import React, { useState, useEffect } from "react";
import { Grid, Typography } from "@mui/material";
import Image from "next/image";
// import { useRouter } from "next/router";
import { SideCards4 } from "../../data/data";
export default function Section4({ blogsCard }) {
  return (
    <>
      <Grid className="home-section-4-main-card">
        <Image
          src="/images/blogimg.png"
          width={400}
          height={300}
          alt=""
          className="home-section-4-blog-img"
        />
        <Grid className="home-section-4-card-text">
          <Typography className="home-section-4-text-1">
            {SideCards4.section4text1}
          </Typography>
          <Grid>
            <Typography className="home-section-4-text-2">
              {SideCards4.section4text2}
            </Typography>
          </Grid>
          <Grid>
            <Typography className="home-section-4-text-3">
              {SideCards4.section4text3}
            </Typography>
          </Grid>
          <Grid className="home-section-4-last-section">
            <Grid style={{ display: "flex", alignItems: "center" }}>
              <Image src={SideCards4.userimage} width={40} height={40} alt="" />
              <Typography className="home-section-4-last-section-text-1">
                {SideCards4.section4text4}
                <Typography className="home-section-4-last-section-text-2">
                  {SideCards4.section4date}
                </Typography>
              </Typography>
            </Grid>
            <Grid>
              <Image src={SideCards4.arrowimg} width={50} height={40} alt="" />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
