import React, { useState } from "react";
import { Grid, Typography, Box } from "@mui/material";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import MenuIcon from "@mui/icons-material/Menu";
import Fade from "@mui/material/Fade";
import Image from "next/image";
import Modal from "react-modal";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import SearchIcon from "@mui/icons-material/Search";
export default function Header() {
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      backgroundColor: "#1c1c1c",
      border: "none",
      borderRadius: "0px",
      width: "35%",
      height: "65%",
      padding: "1% 5%",
    },
  };
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  let subtitle;
  const [modalIsOpen, setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    subtitle.style.color = "#f00";
  }

  function closeModal() {
    setIsOpen(false);
  }
  return (
    <>
      <div className="header-main-div">
        <div className="header-log-div">
          <Image
            src="/images/logo1.png"
            width={250}
            height={70}
            // width={250}
            // height={150}
            className="logo-img-div"
            alt="logo"
          />
        </div>

        <div className="header-content">
          <div className="header-links-1">
            <Typography className="header-link-text-1">Terms of Use</Typography>
            <Typography className="header-link-text-1">KVKK</Typography>
            <Typography className="header-link-text-1">
              Privacy Policy
            </Typography>
            <div>
              <Typography className="header-link-text-1" onClick={openModal}>
                EN
                <ArrowDropDownIcon style={{ color: "rgba(20, 13, 13, 0.3)" }} />
              </Typography>
            </div>
            <SearchIcon style={{ color: "gray" }} />
            <div className="modal-display-none">
              <Modal
                isOpen={modalIsOpen}
                onAfterOpen={afterOpenModal}
                onRequestClose={closeModal}
                style={customStyles}
                contentLabel="Example Modal"
              >
                <h2
                  className="modal-english"
                  ref={(_subtitle) => (subtitle = _subtitle)}
                >
                  English
                </h2>
                <Typography className="modal-language-text">Türkçe</Typography>
                <Typography className="modal-language-text">
                  العربية{" "}
                </Typography>

                <Typography className="modal-language-text">
                  Русский{" "}
                </Typography>

                <button className="modal-close-btn" onClick={closeModal}>
                  Close{" "}
                </button>
              </Modal>
            </div>
          </div>
          <div className="header-links-2">
            <Typography className="header-link-text">HOME</Typography>
            <a href={`/blogPage`}>
              <Typography className="header-link-text">BLOG</Typography>
            </a>
            <Typography className="header-link-text">SERVICES</Typography>
            <Typography className="header-link-text">ABOUT US</Typography>
            <Typography className="header-link-text">CONTACT</Typography>
          </div>
        </div>
        {/* .................................ham............................. */}

        <div className="hamburger">
          <Button
            id="fade-button"
            aria-controls={open ? "fade-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
            onClick={handleClick}
          >
            <MenuIcon className="hamburger-icon" style={{ color: "black" }} />
          </Button>
          <Menu
            id="fade-menu"
            MenuListProps={{
              "aria-labelledby": "fade-button",
            }}
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            TransitionComponent={Fade}
          >
            <MenuItem onClick={handleClose}>Home</MenuItem>
            <a href={`/blogPage`}>
              <MenuItem onClick={handleClose}>Blog</MenuItem>
            </a>
            <MenuItem onClick={handleClose}>Services</MenuItem>
            <MenuItem onClick={handleClose}>About us</MenuItem>
            <MenuItem onClick={handleClose}>Contact</MenuItem>
          </Menu>
        </div>
      </div>
    </>
  );
}
