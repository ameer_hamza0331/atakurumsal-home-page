import React from "react";
import { Grid, Typography, Button, Tooltip } from "@mui/material";
import TurkeyMap from "turkey-map-react";
export default function Map() {
  return (
    <>
      <Grid container className="mapsection-container">
        <Grid item lg={12} className="section2-item-heading3">
          <Typography className="section2-item-text3">
            WORK PERMIT STATISTICS
          </Typography>
        </Grid>

        <Grid className="turkey-map-container" item xs={12}>
          <TurkeyMap
            // hoverable={true}
            // onHover={({ plateNumber, cityName }) =>
            //   console.log(
            //     "Cursor is over on " + plateNumber + " - " + cityName + "!"
            //   )
            // }
            customStyle={{ idleColor: "#d6d6d6", hoverColor: "#E38345" }}
            onClick={({ plateNumber, cityName }) =>
              console.log(plateNumber + " - " + cityName + " is just clicked!")
            }
            cityWrapper={(cityComponent, cityData) => (
              <Tooltip title={cityData.name} key={cityData.id}>
                {cityComponent}
              </Tooltip>
            )}
            hoverable={true}
          />
        </Grid>
      </Grid>
    </>
  );
}
