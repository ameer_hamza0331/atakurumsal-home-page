import React from "react";
import { Grid, Typography } from "@mui/material";
import Image from "next/image";
import Section4 from "../../components/section-4";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import { SideCards4 } from "../../data/data";
import { SideCards3, SideCards3data } from "../../data/data";
export default function section3() {
  // const sideCards2 = [1, 2];

  return (
    <>
      <Grid container mt={6}>
        <Grid item container lg={8} md={12} sm={12} xs={12}>
          {SideCards3.map((props) => (
            <div key={props.id}>
              <div className="home-section-3-main-div">
                <Grid item lg={6}>
                  <Box>
                    <Box>
                      <Image
                        src={props.blogimg}
                        width={350}
                        height={320}
                        className="blog-image-section-3"
                        alt="Section3 Image"
                      />
                    </Box>
                  </Box>
                </Grid>
                <Grid item lg={6}>
                  <Typography className="section-3-card-text-1">
                    {props.section3text1}
                  </Typography>
                  <Typography className="section-3-card-text-2">
                    {props.section3text2}
                  </Typography>
                  <Typography className="section-3-card-text-3">
                    {props.section3text3}
                  </Typography>

                  <Grid className="home-section-3-last-section">
                    <Grid style={{ display: "flex", alignItems: "center" }}>
                      <Image
                        src={props.userimg}
                        width={40}
                        height={40}
                        alt="Image"
                      />
                      <Typography className="home-section-3-last-section-text-1">
                        {props.section3text4}
                        <Typography className="home-section-3-last-section-text-2">
                          {props.section3date}
                        </Typography>
                      </Typography>
                    </Grid>
                    <Grid>
                      <Image
                        src={props.arrowimg}
                        width={70}
                        height={50}
                        alt="Image"
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </div>
          ))}
        </Grid>
        <Grid item lg={4} md={12} sm={12} xs={12} style={{ padding: "10px" }}>
          <Grid className="home-section-3-card-2-div">
            <Box
              sx={{
                width: "auto",
                maxWidth: "100%",
              }}
            >
              <TextField
                className="home-section-3-serach"
                fullWidth
                label="Search"
                id="fullWidth"
              />
            </Box>
            <Grid>
              <Typography className="home-section-3-search-text">
                {SideCards3data.searchbartext}
              </Typography>
            </Grid>
          </Grid>
          <Grid container>
            <Grid
              item
              style={{ justifyContent: "center", display: "flex" }}
              xs={12}
            >
              <Section4 />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
