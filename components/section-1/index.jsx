import React from "react";
import { Grid, Button } from "@mui/material";

export default function section1() {
  return (
    <div>
      <Grid container mt={5}>
        <Grid item lg={1.5} md={1.5} sm={6} xs={12}>
          <Button className="section-1-btn">btn 1</Button>
        </Grid>
        <Grid item lg={1.5} md={1.5} sm={6} xs={12}>
          <Button className="section-1-btn-2">btn 2</Button>
        </Grid>
        <Grid item lg={1.5} md={1.5} sm={6} xs={12}>
          <Button className="section-1-btn-2">btn 3</Button>
        </Grid>
        <Grid item lg={1.5} md={1.5} sm={6} xs={12}>
          <Button className="section-1-btn-2">btn 4</Button>
        </Grid>
        <Grid item lg={1.5} md={1.5} sm={6} xs={12}>
          <Button className="section-1-btn-2">btn 5</Button>
        </Grid>
        <Grid item lg={1.5} md={1.5} sm={6} xs={12}>
          <Button className="section-1-btn-2">btn 6</Button>
        </Grid>
        <Grid item lg={1.5} md={1.5} sm={6} xs={12}>
          <Button className="section-1-btn-2">btn 7</Button>
        </Grid>
        <Grid item lg={1.5} md={1.5} sm={6} xs={12}>
          <Button className="section-1-btn-2">btn 8</Button>
        </Grid>
      </Grid>
    </div>
  );
}
