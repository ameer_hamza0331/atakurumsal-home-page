import React from "react";
import { Grid, Typography } from "@mui/material";
export default function footer() {
  return (
    <>
      <Grid container className="footer-container">
        <Grid item lg={2}>
          <Typography className="footer-heading">Our Services</Typography>
          <Typography className="footer-heading-text">Work Permit</Typography>
          <Typography className="footer-heading-text">
            Residence permit
          </Typography>
          <Typography className="footer-heading-text">
            Turkish citizenship
          </Typography>
          <Typography className="footer-heading-text">
            turquoise card
          </Typography>
          <Typography className="footer-heading-text">company</Typography>
          <Typography className="footer-heading-text">our fees</Typography>
        </Grid>
        <Grid item lg={2}>
          <Typography className="footer-heading">Work Permit</Typography>
          <Typography className="footer-heading-text">Timed</Typography>
          <Typography className="footer-heading-text">INDEFINITE</Typography>
          <Typography className="footer-heading-text">INDEPENDENT</Typography>
          <Typography className="footer-heading-text">GENERAL</Typography>
          <Typography className="footer-heading-text">HOME SERVICES</Typography>
          <Typography className="footer-heading-text">DOCTOR</Typography>
          <Typography className="footer-heading-text">ENGINEER</Typography>
          <Typography className="footer-heading-text">TEACHER</Typography>
          <Typography className="footer-heading-text">PILOT</Typography>
          <Typography className="footer-heading-text">TOURISM</Typography>
          <Typography className="footer-heading-text">STUDENT</Typography>
          <Typography className="footer-heading-text">INTERN</Typography>
          <Typography className="footer-heading-text">
            BUSINESS PARTNER
          </Typography>
          <Typography className="footer-heading-text">
            LIABILITY OFFICES
          </Typography>
          <Typography className="footer-heading-text">TRANSFER</Typography>
          <Typography className="footer-heading-text">FEE FEES</Typography>
        </Grid>
        <Grid item lg={2}>
          <Typography className="footer-heading">RESIDENCE PERMIT</Typography>
          <Typography className="footer-heading-text">timed</Typography>
          <Typography className="footer-heading-text">indefinite</Typography>
          <Typography className="footer-heading-text">independent</Typography>
          <Typography className="footer-heading-text">general</Typography>
          <Typography className="footer-heading-text">home services</Typography>
          <Typography className="footer-heading-text">doctor</Typography>
        </Grid>
        <Grid item lg={2}>
          <Typography className="footer-heading">CITIZENSHIP</Typography>
          <Typography className="footer-heading-text">Work Permit</Typography>
          <Typography className="footer-heading-text">
            Residence permit
          </Typography>
          <Typography className="footer-heading-text">
            Turkish citizenship
          </Typography>
          <Typography className="footer-heading-text">
            turquoise card
          </Typography>
          <Typography className="footer-heading-text">company</Typography>
          <Typography className="footer-heading-text">our fees</Typography>
        </Grid>
        <Grid item lg={2}>
          <Typography className="footer-heading">TURQUOISE CARD</Typography>
          <Typography className="footer-heading-text">Work Permit</Typography>
          <Typography className="footer-heading-text">
            Residence permit
          </Typography>
          <Typography className="footer-heading-text">
            Turkish citizenship
          </Typography>
          <Typography className="footer-heading-text">
            turquoise card
          </Typography>
          <Typography className="footer-heading-text">company</Typography>
          <Typography className="footer-heading-text">our fees</Typography>
        </Grid>
        <Grid item lg={2}>
          <Typography className="footer-heading">BLOG</Typography>
          <Typography className="footer-heading-text">Work Permit</Typography>
          <Typography className="footer-heading-text">
            Residence permit
          </Typography>
          <Typography className="footer-heading-text">
            Turkish citizenship
          </Typography>
          <Typography className="footer-heading-text">
            turquoise card
          </Typography>
          <Typography className="footer-heading-text">company</Typography>
          <Typography className="footer-heading-text">our fees</Typography>
        </Grid>
      </Grid>
    </>
  );
}
