import React from "react";
import Header from "../../components/header";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import YouTubeIcon from "@mui/icons-material/YouTube";
import InstagramIcon from "@mui/icons-material/Instagram";
import FacebookIcon from "@mui/icons-material/Facebook";
import { Grid, Typography, Button } from "@mui/material";
import TurkeyMap from "../../components/turkeyMap";
// import TurkishMap from "../../public/images/turkish-map.html";
import Image from "next/image";
import Slider from "react-slick";
import CountUp from "react-countup";
import {
  Section1,
  Section3,
  Section5,
  Section6,
  Section7,
  Section10,
} from "../../data/data";
export default function index() {
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    arrows: true,
    autoplay: true,
    cssEase: "linear",
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  var settings2 = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    arrows: true,
    autoplay: true,
    cssEase: "linear",
    autoplaySpeed: 2000,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  var settings3 = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    arrows: true,
    autoplay: true,
    cssEase: "linear",
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  var settings4 = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    arrows: true,
    autoplay: true,
    cssEase: "linear",
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  var settings5 = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    arrows: true,
    autoplay: true,
    cssEase: "linear",
    autoplaySpeed: 2000,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", background: "green" }}
        onClick={onClick}
      />
    );
  }

  return (
    <>
      <Header />
      {/* ....................section1.................... */}

      <Grid container className="section1-container">
        <Grid item lg={3} md={3} sm={6} xs={6} className="section1-items">
          <Image src="/images/img1.png" width={70} height={70} alt="hi" />
          <Typography className="section1-text-1">
            <CountUp end={12000} duration={4} />
          </Typography>
          <Typography className="section1-text-2">Work Permit</Typography>
        </Grid>
        <Grid item lg={3} md={3} sm={6} xs={6} className="section1-items">
          <Image src="/images/img2.png" width={70} height={70} alt="hi" />
          <Typography className="section1-text-1">
            <CountUp end={15000} duration={4} />
          </Typography>
          <Typography className="section1-text-2">Residence permit</Typography>
        </Grid>
        <Grid item lg={3} md={3} sm={6} xs={6} className="section1-items">
          <Image src="/images/img3.png" width={70} height={70} alt="hi" />
          <Typography className="section1-text-1">
            <CountUp end={590} duration={4} />
          </Typography>
          <Typography className="section1-text-2">
            Turkish Citizenship
          </Typography>
        </Grid>
        <Grid item lg={3} md={3} sm={6} xs={6} className="section1-items">
          <Image src="/images/img4.png" width={70} height={70} alt="hi" />
          <Typography className="section1-text-1">
            <CountUp end={5} duration={4} />
          </Typography>
          <Typography className="section1-text-2">
            Separate Language Service
          </Typography>
        </Grid>
      </Grid>

      {/* ....................section1.................... */}

      {/* ....................section2.................... */}

      <Grid container mt={10}>
        <Grid item container lg={6} className="section2-item">
          <Grid item xs={12} className="section2-item-heading">
            <Typography className="section2-item-text">
              OUR SPECIALIZATIONS
            </Typography>
          </Grid>
          <Grid item lg={6} className="section2-item-container">
            <Grid className="section2-img-1">
              <Button className="section2-button">
                <Typography className="section2-btn-text">
                  Work permit
                </Typography>
              </Button>
              <Grid className="hover-text">
                <Typography className="hover-heading">
                  We are the first preferred company in work
                  <Typography className="hover-heading">
                    Permit consultancy for foreigners.
                  </Typography>
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item lg={6} className="section2-item-container">
            <Grid className="section2-img-2">
              <Button className="section2-button">
                <Typography className="section2-btn-text">
                  RESIDENCE PERMIT
                </Typography>
              </Button>
              <Grid mt={1} className="hover-text">
                <Typography className="hover-heading">
                  faimly,student,permanent etc.we provide
                  <Typography className="hover-heading">
                    consultancy on all residence permits in
                    <Typography className="hover-heading">turkey.</Typography>
                  </Typography>
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item lg={6} className="section2-item-container">
            <Grid className="section2-img-3">
              <Button className="section2-button">
                <Typography className="section2-btn-text">
                  TURQUOISE CARD
                </Typography>
              </Button>
              <Grid mt={2} className="hover-text">
                <Typography className="hover-heading">
                  what should you pay attention to when
                  <Typography className="hover-heading">
                    applying for a turquoise card? what should
                    <Typography className="hover-heading">
                      you not do?Leave the procedures to us.
                    </Typography>
                  </Typography>
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item lg={6} className="section2-item-container">
            <Grid className="section2-img-4">
              <Button className="section2-button">
                <Typography className="section2-btn-text">
                  TURKISH CITIZENSHIP
                </Typography>
              </Button>
              <Grid mt={4} className="hover-text">
                <Typography className="hover-heading">
                  Investment, employment, marriage, etc. do
                  <Typography className="hover-heading">
                    Do not deal with procedures in obtaining
                    <Typography className="hover-heading">
                      Turkish Citizenship with methods. trust our
                      <Typography className="hover-heading">
                        expertise.
                      </Typography>
                    </Typography>
                  </Typography>
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid className="section2-border-main">
            <Grid className="section2-border-bottom"></Grid>
          </Grid>

          <Grid className="section2-btn-main">
            <Button className="section2-view-all-btn">View All</Button>
          </Grid>
        </Grid>

        {/* ................................................... */}

        <Grid item container lg={6} className="section2-item">
          <Grid item lg={12} className="section2-item-heading2">
            <Typography className="section2-item-text">
              WHY ATA CORPORATE?
            </Typography>
          </Grid>
          <Grid
            item
            lg={6}
            md={6}
            sm={12}
            xs={12}
            className="section2-img-div-2"
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
              className="SPECIALITY"
            >
              <Image src="/images/img5.png" width={35} height={35} alt="hi" />
              <Typography className="section2-2-text">SPECIALITY</Typography>
            </div>
            <Grid className="section2-display">
              <Typography className="hover-text-2">
                All of our staff are experts in their field, five different
                languages ​​are spoken and written fluently in our company.
              </Typography>
            </Grid>
          </Grid>

          <Grid
            item
            lg={6}
            md={6}
            sm={12}
            xs={12}
            className="section2-img-div-2"
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
              className="SPECIALITY"
            >
              <Image src="/images/img6.png" width={35} height={35} alt="hi" />
              <Typography className="section2-2-text">DISCIPLINE</Typography>
            </div>
            <Grid className="section2-display">
              <Typography className="hover-text-2">
                Our readers and procedures are clear. Thats why those who work
                with us are always one step ahead.
              </Typography>
            </Grid>
          </Grid>

          <Grid
            item
            lg={6}
            md={6}
            sm={12}
            xs={12}
            className="section2-img-div-4"
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
              className="INSTITUTIONALITY"
            >
              <Image src="/images/img7.png" width={35} height={35} alt="hi" />
              <Typography className="section2-2-text">
                INSTITUTIONALITY
              </Typography>
            </div>
            <Grid className="section2-display">
              <Typography className="hover-text-2">
                All of our staff are experts in their field, five different
                languages ​​are spoken and written fluently in our company.
              </Typography>
            </Grid>
          </Grid>

          <Grid
            item
            lg={6}
            md={6}
            sm={12}
            xs={12}
            className="section2-img-div-2"
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
              className="SPECIALITY"
            >
              <Image src="/images/img8.png" width={35} height={35} alt="hi" />
              <Typography className="section2-2-text">COMPETENCY</Typography>
            </div>
            <Grid className="section2-display">
              <Typography className="hover-text-2">
                We do not go beyond the rules, we take quick decisions and
                implement them to experienced departments.
              </Typography>
            </Grid>
          </Grid>

          <Grid
            item
            lg={6}
            md={6}
            sm={12}
            xs={12}
            className="section2-img-div-4"
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
              className="INSTITUTIONALITY"
            >
              <Image src="/images/img9.png" width={35} height={40} alt="hi" />
              <Typography className="section2-2-text">
                RESPONSIBILITY
              </Typography>
            </div>
            <Grid className="section2-display">
              <Typography className="hover-text-2">
                We have the experience and strength to manage large projects.
              </Typography>
            </Grid>
          </Grid>

          <Grid
            item
            lg={6}
            md={6}
            sm={12}
            xs={12}
            className="section2-img-div-2"
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
              className="SPECIALITY"
            >
              <Image src="/images/img10.png" width={45} height={35} alt="hi" />
              <Typography className="section2-2-text">CREATIVITY</Typography>
            </div>
            <Grid className="section2-display">
              <Typography className="hover-text-2">
                In our transactions, we use innovative and creative approaches
                that you cannot read or come across anywhere.
              </Typography>
            </Grid>
          </Grid>

          <Grid className="section2-border-main">
            <Grid className="section2-border-bottom"></Grid>
          </Grid>
          <Grid className="section2-btn-main">
            <Button className="section2-view-all-btn">Contact Us</Button>
          </Grid>
        </Grid>
      </Grid>

      {/* ....................section2.................... */}

      {/* ........................section3.................... */}
      <Grid className="section-3-container">
        <Grid container className="section-2-display-flex">
          <Grid item lg={5} className="section2-item-heading3">
            <Typography className="section2-item-text3">
              TURKISH CITIZENSHIP
            </Typography>
          </Grid>
          <Grid item lg={5}>
            <Grid className="section2-btn-main">
              <Button className="section2-view-all-btn">View</Button>
            </Grid>
          </Grid>
        </Grid>

        <div>
          <Slider {...settings}>
            {Section3.map((data3) => (
              <div key={data3.id}>
                <div className="section-3-main-div">
                  <Grid className="section3-img-1">
                    <Image
                      src={data3.Section3Img}
                      width={130}
                      height={120}
                      className="section3-img1-main"
                      alt="hi"
                    />
                  </Grid>
                  <Grid className="section3-text-div-main">
                    <Typography className="section3-text-1">
                      {data3.Section3Text1}
                    </Typography>
                    <Typography className="section3-text-2">
                      {data3.Section3Text2}
                    </Typography>
                  </Grid>
                </div>
              </div>
            ))}
          </Slider>
        </div>
      </Grid>

      {/* ........................section3.................... */}

      {/* .............................section4.................... */}

      <Grid container mt={10} className="section-4-container">
        <Grid item lg={4} md={4} sm={12} xs={12} className="section4-item-1">
          <Typography className="section2-item-text4">
            OUR SOCIAL MEDIA ACCOUNTS
          </Typography>
          <Typography className="section4-item-text-4">
            Follow ATA Corporate on social media accounts and be informed about
            the latest issues.
          </Typography>
        </Grid>
        <Grid item lg={2} md={2} sm={6} xs={12} className="section4-item-2">
          <div>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <LinkedInIcon className="linkedin-icon" />
            </div>
            <div>
              <Typography className="linkedin-text">Linkedin</Typography>
            </div>
          </div>
        </Grid>
        <Grid item lg={2} md={2} sm={6} xs={12} className="section4-item-3">
          <div>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <YouTubeIcon className="youtube-icon" />
            </div>
            <div>
              <Typography className="linkedin-text">Youtube</Typography>
            </div>
          </div>
        </Grid>
        <Grid item lg={2} md={2} sm={6} xs={12} className="section4-item-4">
          <div>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <InstagramIcon className="instagram-icon" />
            </div>
            <div>
              <Typography className="linkedin-text">Instagram</Typography>
            </div>
          </div>
        </Grid>
        <Grid item lg={2} md={2} sm={6} xs={12} className="section4-item-5">
          <div>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <FacebookIcon className="facebook-icon" />
            </div>
            <div>
              <Typography className="linkedin-text">Facebook</Typography>
            </div>
          </div>
        </Grid>
      </Grid>

      {/* .............................section4.................... */}

      {/* ........................section5.................... */}

      <Grid className="section-3-container">
        <Grid
          container
          style={{ justifyContent: "space-between", display: "flex" }}
        >
          <Grid
            item
            lg={5}
            md={6}
            sm={12}
            xs={12}
            className="section2-item-heading3"
          >
            <Typography className="section2-item-text3">BLOG</Typography>
          </Grid>

          <Grid
            item
            lg={5}
            md={6}
            sm={12}
            xs={12}
            className="section2-btn-main"
          >
            <Button className="section2-view-all-btn">View</Button>
          </Grid>
        </Grid>

        <div>
          <Slider {...settings2}>
            {Section5.map((data4) => (
              <Grid key={data4.id} className="section-3-main-div">
                <Grid className="section3-img-1">
                  <Image
                    src={data4.Section4Img}
                    width={130}
                    height={120}
                    className="section3-img1-main"
                    alt="hi"
                  />
                </Grid>
                <Grid className="section3-text-div-main">
                  <Typography className="section3-text-1">
                    {data4.Section5Text1}
                  </Typography>
                  <Typography className="section3-text-2">
                    {data4.Section5Text2}
                  </Typography>
                </Grid>
              </Grid>
            ))}
          </Slider>
        </div>
      </Grid>

      {/* ........................section5.................... */}

      {/* ............................section6....................... */}

      <Grid container className="section6-container">
        <Grid item lg={4} className="section6-item-heading3">
          <Typography className="section6-item-text3">SECTORS</Typography>
          <Typography className="sectors-heading-text">
            Below are some business areas with a large number of foreign
            employees. You can access detailed information and statistics about
            these sectors by clicking.
          </Typography>
        </Grid>
        <Grid item container lg={8} className="section6-imgs-div">
          <Grid item lg={2.4} className="section-6-img">
            <div className="section6-img-text">
              <Image
                src="/images/img11.png"
                width={85}
                height={85}
                alt="hi"
                className="section6-img1"
              />
              <Typography className="section6-text-1">
                {Section6.Text1}
              </Typography>
            </div>
          </Grid>
          <Grid item lg={2.4} className="section-6-img">
            <div className="section6-img-text">
              <Image
                src="/images/img12.png"
                width={85}
                height={85}
                alt="hi"
                className="section6-img1"
              />
              <Typography className="section6-text-1">
                {Section6.Text2}
              </Typography>
            </div>
          </Grid>
          <Grid item lg={2.4} className="section-6-img">
            <div className="section6-img-text">
              <Image
                src="/images/img13.png"
                width={85}
                height={85}
                alt="hi"
                className="section6-img1"
              />
              <Typography className="section6-text-1">
                {Section6.Text3}
              </Typography>
            </div>
          </Grid>
          <Grid item lg={2.4} className="section-6-img">
            <div className="section6-img-text">
              <Image
                src="/images/img14.png"
                width={85}
                height={85}
                alt="hi"
                className="section6-img1"
              />
              <Typography className="section6-text-1">
                {Section6.Text4}
              </Typography>
            </div>
          </Grid>
          <Grid item lg={2.4} className="section-6-img">
            <div className="section6-img-text">
              <Image
                src="/images/img15.png"
                width={85}
                height={85}
                alt="hi"
                className="section6-img1"
              />
              <Typography className="section6-text-1">
                {Section6.Text5}
              </Typography>
            </div>
          </Grid>

          <Grid item lg={2.4} className="section-6-img">
            <div className="section6-img-text">
              <Image
                src="/images/img16.png"
                width={85}
                height={85}
                alt="hi"
                className="section6-img1"
              />
              <Typography className="section6-text-1">
                {Section6.Text6}
              </Typography>
            </div>
          </Grid>
          <Grid item lg={2.4} className="section-6-img">
            <div className="section6-img-text">
              <Image
                src="/images/img17.png"
                width={85}
                height={85}
                alt="hi"
                className="section6-img1"
              />
              <Typography className="section6-text-1">
                {Section6.Text7}
              </Typography>
            </div>
          </Grid>
          <Grid item lg={2.4} className="section-6-img">
            <div className="section6-img-text">
              <Image
                src="/images/img18.png"
                width={85}
                height={85}
                alt="hi"
                className="section6-img1"
              />
              <Typography className="section6-text-1">
                {Section6.Text8}
              </Typography>
            </div>
          </Grid>
          <Grid item lg={2.4} className="section-6-img">
            <div className="section6-img-text">
              <Image
                src="/images/img19.png"
                width={85}
                height={85}
                alt="hi"
                className="section6-img1"
              />
              <Typography className="section6-text-1">
                {Section6.Text9}
              </Typography>
            </div>
          </Grid>
          <Grid item lg={2.4} className="section-6-img2">
            <div className="section6-img-text">
              <Image
                src="/images/img20.png"
                width={85}
                height={85}
                alt="hi"
                className="section6-img1"
              />
              <Typography className="section6-text-1">
                {Section6.Text10}
              </Typography>
            </div>
          </Grid>
        </Grid>
      </Grid>

      {/* ............................section6....................... */}

      {/* ........................section7.................... */}

      <Grid className="section-3-container">
        <Grid
          container
          style={{ justifyContent: "space-between", display: "flex" }}
        >
          <Grid
            item
            lg={5}
            md={5}
            sm={6}
            xs={12}
            className="section2-item-heading3"
          >
            <Typography className="section2-item-text3">WORK PERMIT</Typography>
          </Grid>
          <Grid item lg={5} md={4} sm={6} xs={12}>
            <Grid className="section2-btn-main">
              <Button className="section2-view-all-btn">View</Button>
            </Grid>
          </Grid>
        </Grid>
        <div>
          <Slider {...settings3}>
            {Section7.map((data7) => (
              <Grid key={data7.id} className="section-3-main-div">
                <Grid className="section3-img-1">
                  <Image
                    src={data7.Section5Img}
                    className="section3-img1-main"
                    width={130}
                    height={120}
                    alt="hi"
                  />
                </Grid>
                <Grid className="section3-text-div-main">
                  <Typography className="section3-text-1">
                    {data7.Section7Text1}
                  </Typography>
                  <Typography className="section3-text-2">
                    {data7.Section7Text2}
                  </Typography>
                </Grid>
              </Grid>
            ))}
          </Slider>
        </div>
      </Grid>

      {/* ........................section7.................... */}

      {/* ........................section8.................... */}

      <Grid className="section-3-container">
        <Grid
          item
          lg={12}
          md={12}
          sm={12}
          xs={12}
          className="section2-item-heading3"
        >
          <Typography className="section2-item-text3">
            OUR INFORMATIVE VIDEOS
          </Typography>
        </Grid>
        <div>
          <Slider {...settings4}>
            <Grid className="section-8-main-div">
              <Grid className="section3-img-1">
                <Image
                  src="/images/video1.webp"
                  width={240}
                  height={150}
                  alt="hi"
                  className="section3-img1-main"
                />
              </Grid>
              <Grid className="section3-text-div-main">
                <Typography className="section3-text-1">
                  JUNE 24,2021
                </Typography>
                <Typography className="section3-text-2">
                  How to apply citizenship?
                </Typography>
              </Grid>
            </Grid>
            <Grid className="section-8-main-div">
              <Grid className="section3-img-1">
                <Image
                  src="/images/video2.webp"
                  width={240}
                  height={150}
                  alt="hi"
                  className="section3-img1-main"
                />
              </Grid>
              <Grid className="section3-text-div-main">
                <Typography className="section3-text-1">
                  JUNE 24,2021
                </Typography>
                <Typography className="section3-text-2">
                  How to apply citizenship?
                </Typography>
              </Grid>
            </Grid>
            <Grid className="section-8-main-div">
              <Grid className="section3-img-1">
                <Image
                  src="/images/video3.webp"
                  width={240}
                  height={150}
                  alt="hi"
                  className="section3-img1-main"
                />
              </Grid>
              <Grid className="section3-text-div-main">
                <Typography className="section3-text-1">
                  JUNE 24,2021
                </Typography>
                <Typography className="section3-text-2">
                  How to apply citizenship?
                </Typography>
              </Grid>
            </Grid>

            <Grid className="section-8-main-div">
              <Grid className="section3-img-1">
                <Image
                  src="/images/video1.webp"
                  width={240}
                  height={150}
                  alt="hi"
                  className="section3-img1-main"
                />
              </Grid>
              <Grid className="section3-text-div-main">
                <Typography className="section3-text-1">
                  JUNE 24,2021
                </Typography>
                <Typography className="section3-text-2">
                  How to apply citizenship?
                </Typography>
              </Grid>
            </Grid>
            <Grid className="section-8-main-div">
              <Grid className="section3-img-1">
                <Image
                  src="/images/video3.webp"
                  width={240}
                  height={150}
                  alt="hi"
                  className="section3-img1-main"
                />
              </Grid>
              <Grid className="section3-text-div-main">
                <Typography className="section3-text-1">
                  JUNE 24,2021
                </Typography>
                <Typography className="section3-text-2">
                  How to apply citizenship?
                </Typography>
              </Grid>
            </Grid>
          </Slider>
        </div>
      </Grid>

      {/* ........................section8.................... */}

      <Grid container className="map-display-none">
        <Grid item xs={12}>
          <TurkeyMap />
        </Grid>
      </Grid>
      {/* <Grid container className="mapsection-container">
        <Grid item lg={12} className="section2-item-heading3">
          <Typography className="section2-item-text3">
            WORK PERMIT STATISTICS
          </Typography>
        </Grid>

        <Grid className="turkey-map-container" item xs={12}>
          <img src={TurkishMap} alt="logo" width={500} height={500} />
        </Grid>
      </Grid> */}

      {/* ....................section9.................... */}

      <Grid container mt={10}>
        <Grid item container lg={12} className="section2-item">
          <Grid item xs={12} className="section2-item-heading">
            <Typography className="section2-item-text">
              THINGS TO KNOW
            </Typography>
          </Grid>

          <Grid item lg={6} className="section2-item-container">
            <Grid className="section2-img-1">
              <Button className="section2-button">
                <Typography className="section2-btn-text">
                  Work permit
                </Typography>
              </Button>
              <Grid className="hover-text">
                <Typography className="hover-heading">
                  We are the first preferred company in work
                  <Typography className="hover-heading">
                    Permit consultancy for foreigners.
                  </Typography>
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item lg={6} className="section2-item-container">
            <Grid className="section2-img-2">
              <Button className="section2-button">
                <Typography className="section2-btn-text">
                  RESIDENCE PERMIT
                </Typography>
              </Button>
              <Grid mt={1} className="hover-text">
                <Typography className="hover-heading">
                  faimly,student,permanent etc.we provide
                  <Typography className="hover-heading">
                    consultancy on all residence permits in
                    <Typography className="hover-heading">turkey.</Typography>
                  </Typography>
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item lg={6} className="section2-item-container">
            <Grid className="section2-img-3">
              <Button className="section2-button">
                <Typography className="section2-btn-text">
                  TURQUOISE CARD
                </Typography>
              </Button>
              <Grid mt={2} className="hover-text">
                <Typography className="hover-heading">
                  what should you pay attention to when
                  <Typography className="hover-heading">
                    applying for a turquoise card? what should
                    <Typography className="hover-heading">
                      you not do?Leave the procedures to us.
                    </Typography>
                  </Typography>
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item lg={6} className="section2-item-container">
            <Grid className="section2-img-4">
              <Button className="section2-button">
                <Typography className="section2-btn-text">
                  TURKISH CITIZENSHIP
                </Typography>
              </Button>
              <Grid mt={4} className="hover-text">
                <Typography className="hover-heading">
                  Investment, employment, marriage, etc. do
                  <Typography className="hover-heading">
                    Do not deal with procedures in obtaining
                    <Typography className="hover-heading">
                      Turkish Citizenship with methods. trust our
                      <Typography className="hover-heading">
                        expertise.
                      </Typography>
                    </Typography>
                  </Typography>
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      {/* ....................section9.................... */}

      {/* ........................section10.................... */}

      <Grid className="section-3-container">
        <Grid
          container
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          <Grid
            item
            lg={5}
            md={8}
            sm={6}
            xs={12}
            className="section2-item-heading3"
          >
            <Typography className="section2-item-text3">
              RESIDENCE PERMIT
            </Typography>
          </Grid>
          <Grid item lg={5} md={4} sm={6} xs={12}>
            <Grid className="section2-btn-main">
              <Button className="section2-view-all-btn">View</Button>
            </Grid>
          </Grid>
        </Grid>
        <div>
          <Slider {...settings5}>
            {Section10.map((data10) => (
              <Grid key={data10.id} className="section-3-main-div">
                <Grid className="section3-img-1">
                  <Image
                    src={data10.Section10Img}
                    className="section3-img1-main"
                    width={130}
                    height={120}
                    alt="hi"
                  />
                </Grid>
                <Grid className="section3-text-div-main">
                  <Typography className="section3-text-1">
                    {data10.Section10Text1}
                  </Typography>
                  <Typography className="section3-text-2">
                    {data10.Section10Text2}
                  </Typography>
                </Grid>
              </Grid>
            ))}
          </Slider>
        </div>
      </Grid>

      {/* ........................section10.................... */}
    </>
  );
}
