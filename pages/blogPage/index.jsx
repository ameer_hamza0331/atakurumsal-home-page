import React, { useState, useEffect } from "react";
import Section1 from "../../components/section-1";
import Section2 from "../../components/section-2";
import Section3 from "../../components/section-3";
import Section4 from "../../components/section-4";
import { Grid, Container } from "@mui/material";
import axios from "axios";
import { Pagination } from "@mui/material";
export default function Index() {
  const [blogdata, setBlogData] = useState();
  const [totalBlogs, setTotalBlogs] = useState();
  // const [loader, setLoader] = useState(false);
  useEffect(() => {
    // setLoader(true);
    console.log("hello from useEffect");
    axios
      .get(`https://atakurumsai.herokuapp.com/blog/getAllBlog/1`)
      .then((response) => {
        console.log("response.data", response.data.data.blogData);
        setBlogData(response.data.data.blogData);
        setTotalBlogs(response.data.data.blogCount);
      })
      .catch((error) => {
        // setLoader(false);
        console.log("error", error);
      });
  }, []);
  console.log(blogdata);
  const Total = Math.ceil(totalBlogs / 6);
  return (
    <div>
      <Container>
        <Section1 />
        <Section2 />
        <Section3 />
        <Grid container>
          {blogdata?.map((data) => (
            <div key={data.id}>
              <Grid
                item
                lg={4}
                md={6}
                sm={12}
                xs={12}
                style={{ justifyContent: "center", display: "flex" }}
              >
                <Section4 blogsCard={data} />
              </Grid>
            </div>
          ))}
        </Grid>

        <div className="border-bottom-main">
          <div className="border-bottom"></div>
        </div>

        <div className="pagination">
          <Pagination count={Total} color="primary" shape="rounded" />
        </div>
      </Container>
    </div>
  );
}
