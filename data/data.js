export const Section3 = [
  {
    Section3Img: "/images/section3img1.jpg",
    Section3Text1: "JUNE 24,2021",
    Section3Text2: "How to apply citizenship?",
  },
  {
    Section3Img: "/images/section3img2.jpg",
    Section3Text1: "NOV 24,2021",
    Section3Text2: "Citizenship",
  },
  {
    Section3Img: "/images/section3img3.jpg",
    Section3Text1: "NOV 24,2021",
    Section3Text2: "Turkish Citizenship",
  },
  {
    Section3Img: "/images/section3img2.jpg",
    Section3Text1: "NOV 24,2021",
    Section3Text2: "Turkish Citizenship",
  },
  {
    Section3Img: "/images/section3img3.jpg",
    Section3Text1: "NOV 24,2021",
    Section3Text2: "Citizenship",
  },
  {
    Section3Img: "/images/section3img1.jpg",
    Section3Text1: "AUG 24,2021",
    Section3Text2: "How to apply citizenship?",
  },
  {
    Section3Img: "/images/section3img2.jpg",
    Section3Text1: "NOV 24,2021",
    Section3Text2: "Turkish Citizenship",
  },
];

export const Section5 = [
  {
    Section4Img: "/images/section2img1.jpg",
    Section5Text1: "JUNE 24,2021",
    Section5Text2: "How to apply citizenship?",
  },
  {
    Section4Img: "/images/section2img2.jpg",
    Section5Text1: "NOV 24,2021",
    Section5Text2: "Turkish Citizenship",
  },
  {
    Section4Img: "/images/section2img3.jpg",
    Section5Text1: "AUG 24,2021",
    Section5Text2: "How to apply citizenship?",
  },
  {
    Section4Img: "/images/section2img2.jpg",
    Section5Text1: "NOV 24,2021",
    Section5Text2: "Turkish Citizenship",
  },
  {
    Section4Img: "/images/section2img1.jpg",
    Section5Text1: "AUG 24,2021",
    Section5Text2: "How to apply citizenship?",
  },
];

export const Section6 = {
  Text1: "TOURISM",
  Text2: "TRADE",
  Text3: "SERVICE",
  Text4: "TEXTILE",
  Text5: "GUIDE",
  Text6: "INDUSTRY",
  Text7: "EDUCATION",
  Text8: "HEALTH",
  Text9: "LOGISTICS",
  Text10: "MEDIA",
};

export const Section7 = [
  {
    Section5Img: "/images/section2img2.jpg",
    Section7Text1: "JUNE 24,2021",
    Section7Text2: "How to apply citizenship?",
  },
  {
    Section5Img: "/images/section2img3.jpg",
    Section7Text1: "NOV 24,2021",
    Section7Text2: "Turkish Citizenship",
  },
  {
    Section5Img: "/images/section2img1.jpg",
    Section7Text1: "AUG 24,2021",
    Section7Text2: "How to apply citizenship??",
  },
  {
    Section5Img: "/images/section2img2.jpg",
    Section7Text1: "AUG 24,2021",
    Section7Text2: "How to apply citizenship??",
  },
  {
    Section5Img: "/images/section2img3.jpg",
    Section7Text1: "AUG 24,2021",
    Section7Text2: "How to apply citizenship??",
  },
];

export const Section10 = [
  {
    Section10Img: "/images/section3img1.jpg",
    Section10Text1: "JUNE 24,2021",
    Section10Text2: "How to apply citizenship?",
  },
  {
    Section10Img: "/images/section3img2.jpg",

    Section10Text1: "NOV 24,2021",
    Section10Text2: "Turkish Citizenship",
  },
  {
    Section10Img: "/images/section3img3.jpg",

    Section10Text1: "AUG 24,2021",
    Section10Text2: "How to apply citizenship?",
  },

  {
    Section10Img: "/images/section3img1.jpg",

    Section10Text1: "AUG 24,2021",
    Section10Text2: "How to apply citizenship?",
  },
];

export const SideCards = [
  {
    userimage: "/images/user.png",
    section2text1:
      " Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",
    section2text2: "   3 gün önce",
  },

  {
    userimage: "/images/user.png",
    section2text1:
      " Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",
    section2text2: "   3 gün önce",
  },
  {
    userimage: "/images/user.png",
    section2text1:
      " Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",
    section2text2: "   3 gün önce",
  },
];

export const SideCards2 = {
  bannertext:
    "  Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",

  buttontext: "Devamına Göz At",
};

export const SideCards3data = {
  searchbartext:
    " Türkiye’de covid-19, çalışma izni nasıl alınır?, evlilik yoluyla vatandaşlık başvurusu, Türkiye’de geri gönderme merkezi, yatırım yoluyla Türk vatandaşlığı",
};

export const SideCards4 = {
  section4text1: "  Ameer hamza",
  section4text2: "my name is ameerhamza",
  section4text3: " ndkjfnjdfn",
  userimage: "/images/user.png",
  section4text4: "     Kategori: Türkiye'de Sağlık",
  section4date: "       26 Ocak 2022",
  arrowimg: "/image/arrowimg.svg",
};

export const SideCards3 = [
  {
    blogimg: "/images/blogimg.png",
    section3text1:
      " Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",
    section3text2: "     Kategori: Türkiye'de Sağlık",
    section3text3:
      "     Türkiye’de Covid-19 ile mücadele kapsamında 1 yılı aşkın süredir hem Türk vatandaşları hem de yasal olarak Türkiye’debulunan yabancılar aşı olma hakkında sahipti. Bu kişiler, en yakın sağlık kuruluşlarından randevu alarak aşılarını kolayca ve ücretsiz olarak olabiliyordu... Bu Kişiler, en yakın sağlık kuruluşlarından randevu alarak...",
    userimg: "/images/user.png",
    section3text4: "     Kategori: Türkiye'de Sağlık",
    section3date: "       26 Ocak 2022",
    arrowimg: "/image/arrowimg.svg",
  },

  {
    blogimg: "/images/blogimg.png",
    section3text1:
      " Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",
    section3text2: "     Kategori: Türkiye'de Sağlık",
    section3text3:
      "     Türkiye’de Covid-19 ile mücadele kapsamında 1 yılı aşkın süredir hem Türk vatandaşları hem de yasal olarak Türkiye’debulunan yabancılar aşı olma hakkında sahipti. Bu kişiler, en yakın sağlık kuruluşlarından randevu alarak aşılarını kolayca ve ücretsiz olarak olabiliyordu... Bu Kişiler, en yakın sağlık kuruluşlarından randevu alarak...",
    userimg: "/images/user.png",
    section3text4: "     Kategori: Türkiye'de Sağlık",
    section3date: "       26 Ocak 2022",
    arrowimg: "/image/arrowimg.svg",
  },
];
